Mit GetYourDailyWeather kann man den 7 Tages Wetterbericht einer Stadt abrufen.
    Hierfür gibt man in das Formular Feld: "Stadt, Land" ein und betätigt den Sucht-Button
    
Nun wird der erste Service ausgeführt der aus der angegebenen Stadt und dem dazugehörigen Land, die Latitude und Longitude raussucht.
Diese zwei Werte werden dann an den zweiten Service weitergegeben. Dieser gibt den Wetterbericht anhand der Latitude und Longitude heraus.
